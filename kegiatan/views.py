from django.shortcuts import render
from .forms import *

# Create your views here.
def kegiatanView(request):
    context = {}
    form1 = kegiatanForm()
    form2 = orangForm()
    kegiatan = Kegiatan.objects.all()
    context['form1'] = form1
    context['form2'] = form2
    context['kegiatan'] = kegiatan
    if request.method == 'POST':
        form1 = kegiatanForm(request.POST)
        form2 = orangForm(request.POST)
        if form1.is_valid():
            form1.save()
            context['kegiatanSukses'] = True
        elif form2.is_valid():
            kegiatan2 = Kegiatan.objects.get(id=request.POST.get('pkKegiatan'))
            orang = Orang.objects.create(nama=form2.cleaned_data.get('nama'))
            orang.kegiatan.add(kegiatan2)
    return render(request, 'kegiatan.html', context)
