from django import forms
from .models import *

class kegiatanForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = ('namaKegiatan',)

class orangForm(forms.ModelForm):
    class Meta:
        model = Orang
        fields = ('nama',)
