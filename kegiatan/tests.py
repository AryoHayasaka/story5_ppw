from django.test import TestCase, Client
from .models import *
from .forms import *

# Create your tests here.
class KegiatanTest(TestCase):

    def test_kegiatan_url_exist(self):
        response = Client().get("/kegiatan/")
        self.assertEquals(response.status_code, 200)

    def test_kegiatan_template(self):
        response = Client().get("/kegiatan/")
        self.assertTemplateUsed(response, 'kegiatan.html')
    
    def test_kegiatan_template_complete(self):
        response = Client().get("/kegiatan/")
        html_response = response.content.decode('utf8')
        self.assertIn("KEGIATAN", html_response)
        self.assertIn("List Kegiatan", html_response)
        self.assertIn("Daftar Kegiatan", html_response)

    def test_kegiatan_model_kegiatan(self):
        kegiatan =  Kegiatan.objects.create(namaKegiatan='LOL')
        hitung = Kegiatan.objects.all().count()
        self.assertEquals(1, hitung)

    def test_kegiatan_model_person(self):
        person = Orang.objects.create(nama = 'LOL')
        hitung = Orang.objects.all().count()
        self.assertEquals(1, hitung)

    def test_kegiatan_form_kegiatan(self):
        form = kegiatanForm(data={'namaKegiatan' : 'LOL'})
        form.save()
        hitung = Kegiatan.objects.all().count()
        self.assertEquals(1, hitung)

    def test_kegiatan_form_person(self):
        form = orangForm(data={'nama' : 'LOL'})
        kegiatan = Kegiatan.objects.create(namaKegiatan='LOL')
        form.save()
        orang = Orang.objects.get(pk=1)
        orang.kegiatan.add(kegiatan)
        hitung = Orang.objects.all().count()
        hitung2 = kegiatan.orang_set.all().count()
        self.assertEquals(1, hitung, hitung2)

    def test_kegiatan_form_kegiatan_post(self):
        response = Client().post('/kegiatan/', {'namaKegiatan':'LOL'})
        hitung = Kegiatan.objects.all().count()
        self.assertEquals(1, hitung)

    def test_kegiatan_form_orang_post(self):
        kegiatan = Kegiatan.objects.create(namaKegiatan='LOL')
        response = Client().post('/kegiatan/', {'nama':'LOL', 'pkKegiatan' : 1})
        hitung = Orang.objects.all().count()
        self.assertEquals(1, hitung)