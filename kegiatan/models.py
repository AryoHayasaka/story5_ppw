from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    namaKegiatan = models.CharField(max_length = 20)

class Orang(models.Model):
    nama = models.CharField(max_length = 20)
    kegiatan = models.ManyToManyField(Kegiatan)