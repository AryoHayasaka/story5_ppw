from django.test import TestCase, Client

# Create your tests here.
class Accrodiontest(TestCase):
    def test_kegiatan_url_exist(self):
        response = Client().get("/accordion/")
        self.assertEquals(response.status_code, 200)

    def test_kegiatan_template(self):
        response = Client().get("/accordion/")
        self.assertTemplateUsed(response, 'accordion.html')
    
    def test_kegiatan_template_complete(self):
        response = Client().get("/accordion/")
        html_response = response.content.decode('utf8')
        self.assertIn("Aktivitas Saat Ini", html_response)
        self.assertIn("Aktivitas Saat Ini Part 2", html_response)
        self.assertIn("Organisasi/Kepanitiaan", html_response)
        self.assertIn("Prestasi", html_response)
