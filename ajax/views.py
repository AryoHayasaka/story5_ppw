from django.shortcuts import render
from django.http import JsonResponse
import requests

# Create your views here.
def ajaxViews(request):
    return render(request, 'ajax.html')

def get_book_request(request):
    query = request.GET.get('query')
    url = f"https://www.googleapis.com/books/v1/volumes?q={query}"
    response = requests.get(url=url)
    return JsonResponse(data=response.json())