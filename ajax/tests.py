from django.test import TestCase, Client

# Create your tests here.
class Ajaxtest(TestCase):
    def test_ajax_url_exist(self):
        response = Client().get("/ajax/")
        self.assertEquals(response.status_code, 200)

    def test_ajax_template(self):
        response = Client().get("/ajax/")
        self.assertTemplateUsed(response, 'ajax.html')

    def test_ajax_json_success(self):
        response = Client().get("/ajax/response/", {'query' : 'kaguya'})
        html_response = response.content.decode('utf8')
        self.assertIn('Aka Akasaka', html_response)
        self.assertIn('Kaguya-sama', html_response)

    def test_ajax_json_not_found(self):
        response = Client().get("/ajax/", {'query' : 'bjkbgyg'})
        html_response = response.content.decode('utf8')
        self.assertIn('Tidak Ditemukan', html_response)

    
        
