from django.urls import path
from .views import *

urlpatterns = [
    path('', ajaxViews),
    path('response/', get_book_request)
]