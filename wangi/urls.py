from django.urls import path
from .views import *

urlpatterns = [
    path('', loginView, name='loginbase'),
    path('success/', loginSuccessView, name='loginsuccess'),
    path('error/', loginErrorView, name='loginerror'),
    path('signup/', signupView, name='signup'),
    path('logout/', logoutView, name='logout')
]