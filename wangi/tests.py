from django.test import TestCase, Client
from django.contrib.auth.models import User

# Create your tests here.
class Wangitest(TestCase):

    def test_wangi_url_exist(self):
        response = Client().get('/wangi/')
        self.assertEqual(response.status_code, 200)

    def test_wangi_correct_template(self):
        response = Client().get('/wangi/')
        self.assertTemplateUsed(response, 'wangi.html')

    def test_wangi_signup_url_exist(self):
        response = Client().get('/wangi/signup/')
        self.assertEqual(response.status_code, 200)

    def test_wangi_signup_correct_template(self):
        response = Client().get('/wangi/signup/')
        self.assertTemplateUsed(response, 'wangi_signup.html')

    def test_wangi_signup_success(self):
        response = Client().post('/wangi/signup/', 
        {'email' : 'rexlapis@archon.com', 'username' : 'zhongli', 'password' : 'guizhong'})
        count = User.objects.all().count()
        self.assertEqual(count, 1)

    def test_wangi_login_success(self):
        self.dummyUser()
        response = Client().post('/wangi/', 
        {'username' : 'zhongli', 'password' : 'guizhong'})
        self.assertRedirects(response, '/wangi/success/')

    def test_wangi_login_fail(self):
        self.dummyUser()
        response = Client().post('/wangi/', 
        {'username' : 'zhongli', 'password' : 'salt'})
        self.assertRedirects(response, '/wangi/error/')

    def dummyUser(self):
        user = User.objects.create_user('zhongli','rexlapis@archon.com', 'guizhong')



    

