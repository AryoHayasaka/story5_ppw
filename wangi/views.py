from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

# Create your views here.
def loginView(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('loginsuccess')
        else:
            return redirect('loginerror')
    return render(request, 'wangi.html')

def loginSuccessView(request):
    context = {}
    if request.user.is_authenticated:
        context['success'] = True
    else:
        context['failed'] = True
    return render(request, 'wangi_success.html', context)

def loginErrorView(request):
    return render(request, 'wangi_error.html')

def signupView(request):
    context = {}
    if request.method == 'POST':
        email = request.POST.get('email')
        username = request.POST.get('username')
        password = request.POST.get('password')
        newUser = User.objects.create_user(username, email, password)
        context['success'] = True
    
    return render(request, 'wangi_signup.html', context)

def logoutView(request):
    logout(request)
    return redirect('loginbase')