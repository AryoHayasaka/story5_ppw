from django.shortcuts import render
from .forms import *
from .models import MataKuliah

# Create your views here.

#Terima Kasih StackOverflow
def matkulView(request):
    form = matkulForm()
    form2 = matkulDelete()
    matkul = MataKuliah.objects.all()
    if request.method == 'POST':
        res = matkulForm(request.POST)
        res2 = matkulDelete(request.POST)
        if res.is_valid():
            res.save()
            tambah = True
            return render(request, 'matkul.html', {'form' : form, 'form2' : form2, 'tambah' : tambah, 'matkul': matkul})
        elif res2.is_valid():
            tujuan = MataKuliah.objects.get(pk=res2.cleaned_data['choice'].pk)
            tujuan.delete()
            hapus = True
            return render(request, 'matkul.html', {'form' : form, 'form2' : form2, 'hapus' : hapus, 'matkul': matkul})
    
    
    return render(request, 'matkul.html', {'form' : form, 'form2' : form2, 'matkul': matkul})

def matkulAllView(request, id):
    matkul = MataKuliah.objects.get(id=id)

    return render(request, 'matkul_all.html', {'matkul': matkul})
