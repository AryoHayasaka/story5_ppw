from django import forms
from matkul.models import MataKuliah

class matkulForm(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields = ('namaMatkul', 'dosen', 'sks', 'deskripsi', 'semester','ruangKelas')
        labels = {
            'namaMatkul' : 'Nama Matkul',
            'sks' : 'SKS',
            'ruangKelas' : 'Ruang Kelas'
        }

class matkulDelete(forms.Form):
    choice = forms.ModelChoiceField(queryset= MataKuliah.objects.all())



