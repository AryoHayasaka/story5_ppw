from django.db import models

# Create your models here.
class MataKuliah(models.Model):
    namaMatkul = models.CharField(max_length=99)
    dosen = models.CharField(max_length = 20)
    sks = models.IntegerField()
    deskripsi = models.CharField(max_length=120)
    semester = models.CharField(max_length=15)
    ruangKelas = models.CharField(max_length=7)

    def __unicode__(self):
        return self.pk

    def __str__(self):
        return self.namaMatkul
