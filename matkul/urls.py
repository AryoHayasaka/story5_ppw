from django.urls import path
from .views import *

urlpatterns = [
    path('', matkulView, name='catat'),
    path('all/<int:id>', matkulAllView, name='all')
]